<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
/**
 * @OA\Schema(
 *     title="Model",
 *     description="Car model",
 *     @OA\Xml(
 *         name="Model"
 *     )
 * )
 */
class CarModel extends Model
{
    protected $table = 'models';

    use HasFactory;

    /**
     * @OA\Property(
     *     title="id",
     *     description="Model id",
     *     format="int64",
     *     example=1
     * )
     *
    @var int
     */
    private $id;

    /**
     * @OA\Property(
     *     title="name",
     *     description="Model name",
     *     format="string",
     *     example="Camry"
     * )
     *
    @var string
     */
    private $name;

    /**
     * @OA\Property(
     *     title="mark_id",
     *     description="Mark id",
     *     format="int64",
     *     example=1
     * )
     *
    @var int
     */
    private $mark_id;

    /**
     * @OA\Property(
     *     title="mark",
     *     description="Mark",
     *     format="object",
     * )
     *
    @var \App\Models\Mark
     */
    private $mark;

    /**
     * @OA\Property(
     *     title="created_at",
     *     description="Created datetime"
     * )
     *
    @var datetime
     */
    private $created_at;

    /**
     * @OA\Property(
     *     title="updated_at",
     *     description="Updated datetime"
     * )
     *
    @var datetime
     */
    private $updated_at;

    public function mark(): HasOne
    {
        return $this->hasOne(Mark::class, 'id', 'mark_id');
    }
}
