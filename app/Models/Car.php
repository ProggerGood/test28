<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @OA\Schema(
 *     title="Car",
 *     description="Car",
 *     @OA\Xml(
 *         name="Car"
 *     )
 * )
 */
class Car extends Model
{
    use HasFactory;

    /**
     * @OA\Property(
     *     title="id",
     *     description="Car id",
     *     format="int64",
     *     example=1
     * )
     *
    @var int
     */
    private $id;

    /**
     * @OA\Property(
     *     title="mark_id",
     *     description="Car mark id",
     *     format="int64",
     *     example=1
     * )
     *
    @var int
     */
    private $mark_id;

    /**
     * @OA\Property(
     *     title="model_id",
     *     description="Car model id",
     *     format="int64",
     *     example=1
     * )
     *
    @var int
     */
    private $model_id;

    /**
     * @OA\Property(
     *     title="mark",
     *     description="Mark",
     *     format="object",
     * )
     *
    @var \App\Models\Mark
     */
    private $mark;

    /**
     * @OA\Property(
     *     title="model",
     *     description="Model",
     *     format="object",
     * )
     *
    @var \App\Models\Model
     */
    private $model;

    /**
     * @OA\Property(
     *     title="release_year",
     *     description="Car release year",
     *     format="int64",
     *     example=2003
     * )
     *
    @var int
     */
    private $release_year;

    /**
     * @OA\Property(
     *     title="mileage",
     *     description="Car mileage",
     *     format="int64",
     *     example=100000
     * )
     *
    @var int
     */
    private $mileage;

    /**
     * @OA\Property(
     *     title="color",
     *     description="Car color",
     *     format="string",
     *     example="black"
     * )
     *
    @var string
     */
    private $color;

    /**
     * @OA\Property(
     *     title="created_at",
     *     description="Created datetime"
     * )
     *
    @var datetime
     */
    private $created_at;

    /**
     * @OA\Property(
     *     title="updated_at",
     *     description="Updated datetime"
     * )
     *
    @var datetime
     */
    private $updated_at;

    protected $fillable = ['model_id', 'mark_id', 'release_year', 'mileage', 'color'];

    public function mark(): HasOne
    {
        return $this->hasOne(Mark::class, 'id', 'mark_id');
    }

    public function model(): HasOne
    {
        return $this->hasOne(CarModel::class, 'id', 'model_id');
    }
}
