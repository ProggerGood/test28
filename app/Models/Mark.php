<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
/**
 * @OA\Schema(
 *     title="Mark",
 *     description="Mark model",
 *     @OA\Xml(
 *         name="Mark"
 *     )
 * )
 */
class Mark extends Model
{
    use HasFactory;

    /**
     * @OA\Property(
     *     title="id",
     *     description="Mark id",
     *     format="int64",
     *     example=1
     * )
     *
    @var int
     */
    private $id;

    /**
     * @OA\Property(
     *     title="name",
     *     description="Mark name",
     *     format="string",
     *     example="Toyota"
     * )
     *
    @var string
     */
    private $name;

    /**
     * @OA\Property(
     *     title="created_at",
     *     description="Created datetime"
     * )
     *
    @var datetime
     */
    private $created_at;

    /**
     * @OA\Property(
     *     title="updated_at",
     *     description="Updated datetime"
     * )
     *
    @var datetime
     */
    private $updated_at;
}
