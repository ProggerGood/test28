<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(
 *     title="CarResource",
 *     description="Car resource",
 *     @OA\Xml(
 *         name="CarResource"
 *     )
 * )
 */
class CarResource extends JsonResource
{

    /**
     * @OA\Property(
     *     title="Data",
     *     description="Data wrapper"
     * )
     *
     * @var \App\Models\Car[]
     */

    private $data;

    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'mark_id' => $this->model_id,
            'model_id' => $this->model_id,
            'mark' => $this->mark,
            'model' => $this->model,
            'release_year' => $this->release_year,
            'mileage' => $this->mileage,
            'color' => $this->color,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
