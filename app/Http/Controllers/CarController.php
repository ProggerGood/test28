<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCarRequest;
use App\Http\Resources\CarResource;
use App\Models\Car;
use Illuminate\Http\Request;



class CarController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/cars",
     *      operationId="getCarsList",
     *      tags={"Cars"},
     *      summary="Get list of cars",
     *      description="Returns list of cars",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/CarResource")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function index()
    {
        return CarResource::collection(Car::with(['mark', 'model'])->get()->all());
    }

    /**
     * @OA\Post(
     *      path="/api/cars/create",
     *      operationId="createCar",
     *      tags={"Cars"},
     *      summary="Create car",
     *      description="Create car, returns created car",
     *     @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/StoreCarRequest")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/CarResource")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function create(Request $request)
    {
        $request->validate([
            'mark_id' => 'required',
            'model_id' => 'required'
        ]);

        $car = new Car();

        try {
            $car->fill($request->toArray());
            $car->save();
        } catch (\Exception $e) {
            report($e);
            return $e->getMessage();
        }

        return new CarResource($car);
    }

    /**
     * @OA\Get(
     *      path="/api/cars/{id}",
     *      operationId="showCar",
     *      tags={"Cars"},
     *      summary="Show car",
     *      description="Returns car data by its id",
     *      @OA\Parameter(
     *          name="id",
     *          description="Car id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/CarResource")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function show(int $id)
    {

        $car = Car::find($id);

        if (!$car)
        {
            return 'Car not found.';
        }

        return new CarResource($car);
    }

    /**
     * @OA\Post(
     *      path="/api/cars/update/{id}",
     *      operationId="updateCar",
     *      tags={"Cars"},
     *      summary="Update car",
     *      description="Update car, returns updated car",
     *      @OA\Parameter(
     *          name="id",
     *          description="Car id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/CarResource")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function update(int $id, Request $request)
    {
        $car = Car::find($id);

        if (!$car)
        {
            return 'Car not found.';
        }

        $request->validate([
            'mark_id' => 'required',
            'model_id' => 'required'
        ]);

        try {
            $car->fill($request->toArray());
            $car->save();
        } catch (\Exception $e) {
            report($e);
            return $e->getMessage();
        }

        return response()->json($car->toArray());
    }

    /**
     * @OA\Post(
     *      path="/api/cars/delete/{id}",
     *      operationId="deleteCar",
     *      tags={"Cars"},
     *      summary="Delete car",
     *      description="Delete car, returns ok if deleted",
     *      @OA\Parameter(
     *          name="id",
     *          description="Car id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function delete(int $id)
    {
        $car = Car::find($id);

        if (!$car)
        {
            return 'Car not found.';
        }

        try
        {
            Car::destroy($id);
        }
        catch (\Exception $e)
        {
            report($e);
            return $e->getMessage();
        }

        return 'ok';
    }
}
