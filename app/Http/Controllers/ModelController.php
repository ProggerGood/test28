<?php

namespace App\Http\Controllers;

use App\Http\Resources\CarModelResource;
use App\Models\CarModel;
use Illuminate\Http\Request;

class ModelController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/models",
     *      operationId="getModelsList",
     *      tags={"Models"},
     *      summary="Get list of car models",
     *      description="Returns list of car models",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/CarModelResource")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function index()
    {
        return CarModelResource::collection(CarModel::with('mark')->get()->all());
    }
}
