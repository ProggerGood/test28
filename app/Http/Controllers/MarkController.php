<?php

namespace App\Http\Controllers;

use App\Http\Resources\MarkResource;
use App\Models\Mark;

class MarkController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/marks",
     *      operationId="getMarksList",
     *      tags={"Marks"},
     *      summary="Get list of marks",
     *      description="Returns list of marks",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/MarkResource")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function index()
    {
        return MarkResource::collection(Mark::all());
    }
}
