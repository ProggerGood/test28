<?php

namespace App\Http\Requests;
use Illuminate\Http\Request;

/**
 * @OA\Schema(
 *      title="Login request",
 *      description="Login body data",
 *      type="object",
 *      required={"email", "password"}
 * )
 */
class LoginRequest extends Request
{

    /**
     * @OA\Property(
     *      title="email",
     *      description="User email",
     *      format="string",
     *      example="test@example.com"
     * )
     *
     * @var string
     */
    public $email;

    /**
     * @OA\Property(
     *      title="password",
     *      description="User password",
     *      format="string",
     *      example="test"
     * )
     *
     * @var string
     */
    public $password;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            ['email, password', ['required']]
        ];
    }
}
