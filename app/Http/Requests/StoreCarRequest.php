<?php

namespace App\Http\Requests;
use Illuminate\Http\Request;

/**
 * @OA\Schema(
 *      title="Store Car request",
 *      description="Store Car request body data",
 *      type="object",
 *      required={"mark_id", "model_id"}
 * )
 */
class StoreCarRequest extends Request
{

    /**
     * @OA\Property(
     *      title="mark_id",
     *      description="Mark id",
     *      format="int64",
     *      example=1
     * )
     *
     * @var int
     */
    public $mark_id;

    /**
     * @OA\Property(
     *      title="model_id",
     *      description="Model id",
     *      format="int64",
     *      example=1
     * )
     *
     * @var int
     */
    public $model_id;

    /**
     * @OA\Property(
     *      title="release_year",
     *      description="Car release year",
     *      format="int64",
     *      example=1998
     * )
     *
     * @var int
     */
    public $release_year;

    /**
     * @OA\Property(
     *      title="mileage",
     *      description="Car mileage",
     *     format="int64",
     *      example=150000
     * )
     *
     * @var int
     */
    public $mileage;

    /**
     * @OA\Property(
     *      title="color",
     *      description="Car color",
     *      format="string",
     *      example="black"
     * )
     *
     * @var string
     */
    public $color;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            ['mark_id, model_id', ['required']]
        ];
    }
}
