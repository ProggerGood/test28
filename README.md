## Установка

1.Создаём миграции

    php artisan migrate:install
    
2.Запускаем все миграции    
    
    php artisan migrate

3.Наполняем базу данными

    php artisan db:seed

4.Запускаем приложение
        
    php artisan serve
    
5.Документация по API находится здесь http://localhost:8000/api/documentation     
