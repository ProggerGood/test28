<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\CarModel;
use App\Models\Mark;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use function Symfony\Component\Uid\Factory\create;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        //\App\Models\User::factory(10)->create();

        \App\Models\User::factory()->create([
             'name' => 'Test User',
             'email' => 'test@example.com',
             'password' => Hash::make('test')
        ]);

        $mark = new Mark();
        $mark->name = 'Toyota';
        $mark->save();

        $model = new CarModel();
        $model->name = 'Camry';
        $model->mark_id = $mark->id;
        $model->save();

        DB::table('cars')->insert([
            'mark_id' => $mark->id,
            'model_id' => $model->id,
            'release_year' => 2013,
            'mileage' => 30000,
            'color' => 'white'
        ]);

        $mark = new Mark();
        $mark->name = 'Honda';
        $mark->save();

        $model = new CarModel();
        $model->name = 'Civic';
        $model->mark_id = $mark->id;
        $model->save();

        DB::table('cars')->insert([
            'mark_id' => $mark->id,
            'model_id' => $model->id,
            'release_year' => 2007,
            'mileage' => 50000,
            'color' => 'yellow'
        ]);

        $mark = new Mark();
        $mark->name = 'Mazda';
        $mark->save();

        $model = new CarModel();
        $model->name = 'Demio';
        $model->mark_id = $mark->id;
        $model->save();

        DB::table('cars')->insert([
            'mark_id' => $mark->id,
            'model_id' => $model->id,
            'release_year' => 2023,
            'mileage' => 0,
            'color' => 'red'
        ]);
    }
}
