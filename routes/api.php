<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MarkController;
use App\Http\Controllers\ModelController;
use App\Http\Controllers\CarController;
use App\Http\Controllers\LoginController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', [LoginController::class,'index'])->name('login');

Route::middleware('auth:sanctum')->get('/marks', [MarkController::class,'index']);
Route::middleware('auth:sanctum')->get('/models', [ModelController::class,'index']);

Route::controller(CarController::class)->group(function () {
    Route::get('/cars/{id}', 'show');
    Route::post('/cars/update/{id}', 'update');
    Route::post('/cars/delete/{id}', 'delete');
    Route::get('/cars', 'index');
    Route::post('/cars/create', 'create');
})->middleware('auth:sanctum');
